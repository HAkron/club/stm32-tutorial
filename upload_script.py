Import ("env")

projectdir = env['PROJECT_DIR'].replace(' ', '^ ')
oldsourcefile = "$SOURCE"
sourcefile = oldsourcefile.replace('/', '\\')

env.Replace(
    UPLOADCMD="hid-flash " + sourcefile,
)