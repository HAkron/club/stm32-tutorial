This contains the binaries to the hid cli tools to flash to the stm32.


Below is the source form Github where the tools came from
https://github.com/bootsector/stm32-hid-bootloader

I have included the binaries in this repo for convenience.