#include <mbed.h>

DigitalOut myled(LED1);
SPI eeprom(SPI_MOSI, SPI_MISO, SPI_SCK);
DigitalOut cs(SPI_CS);


//enable_write allows you to write to the chip
//It must be used every time before performing a write operation
void enable_write() {
  cs = 0;
  eeprom.write(0x06); // The Write enable code
  cs = 1;
  wait_ms(100);
}

//erase_sector will erase a 4KB sector and make it ready
//for writing.  Enable write must be called (as shown in the function)
void erase_sector(int32_t address) {
  enable_write();
  cs = 0;
  eeprom.write(0x20); //The erase sector code
  //Write the address
  eeprom.write(address >> 16);
  eeprom.write(address >> 8);
  eeprom.write(address);
  cs = 1;
  wait_ms(100);
}

//write_byte lets you write 1 byte of data to the specified address
//It called enable_write before doing so
void write_byte(int32_t address, int data) {
  enable_write();
  cs = 0;
  eeprom.write(0x02); // the "Page Program" code
  //Write the address
  eeprom.write(address >> 16);
  eeprom.write(address >> 8);
  eeprom.write(address);
  eeprom.write(data); // Write the data
  cs = 1;
  wait_ms(100);

}

//read_byte lets you read 1 byte of data from the specified address
int read_byte(int32_t address) {
  cs = 0;
  eeprom.write(0x03); // The read data code
  //Writ the address
  eeprom.write(address >> 16);
  eeprom.write(address >> 8);
  eeprom.write(address);
  //Write 0x00, and listen to the response to get the result
  //This pulses the clock on the slave so that it sends that next byte of data
  int result = eeprom.write(0x00);
  cs = 1;
  wait_ms(100);
  //Return the result
  return result;
}

//blink_led blinks the LED a set number of times (give by param num)
void blink_led(int num) {
  myled = 1;
  for (int i = 0; i < num; i++) {
    myled = 0;
    wait_ms(500);
    myled = 1;
    wait_ms(500);
  }
}

int main() {
  //Ensure cs is deactivated (by setting it to 1)
  cs = 1;
  //Format SPI for 8 bit data transfer
  eeprom.format(8);
  //Set the transfer frequency to 1 MHz
  eeprom.frequency(1000000);
  //Turn the LED off
  myled = 1;

  //Set your address
  int32_t address = 0x070000;
  
  //Erase the given address
  erase_sector(address);
  //Write 1 byte of data
  write_byte(address, 0x0A);
  //Read the byte of data
  int result = read_byte(address);
  if (result == 0xFF) {
    //Something went wrong, just set the LED to high and exit
    myled = 0;
    return 0;
  }
  //BLINK
  blink_led(result);
}